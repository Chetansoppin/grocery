package com.example.chetanps.grocery.Recycler;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.chetanps.grocery.R;
import com.example.chetanps.grocery.RecyclerAdapter;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by chetanps on 17-03-18.
 */

public class ItemRecyclerAdapter extends RecyclerView.Adapter<ItemsViewHolder>  {
    Context mcontext;
    ArrayList<ItemsAll> itemsAll;
    ItemClickListener itemClickListener;

    public ItemRecyclerAdapter(Context mcontext, ArrayList<ItemsAll> itemsAll) {
        this.mcontext = mcontext;
        this.itemsAll = itemsAll;
    }

    @Override
    public ItemsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_layout ,null);
        ItemsViewHolder holder = new ItemsViewHolder(v ,itemClickListener );
        return holder;
    }

    @Override
    public void onBindViewHolder(ItemsViewHolder holder, int position) {
holder.itemImage.setImageResource(itemsAll.get(position).getItemImage());
holder.itemName.setText(itemsAll.get(position).getItemName());



    }

    @Override
    public int getItemCount() {
        return itemsAll.size();
    }
}
