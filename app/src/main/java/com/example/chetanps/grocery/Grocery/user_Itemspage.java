package com.example.chetanps.grocery.Grocery;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;


import com.example.chetanps.grocery.Fragments.allItemsList;
import com.example.chetanps.grocery.Fragments.oilList;
import com.example.chetanps.grocery.Fragments.seedsList;
import com.example.chetanps.grocery.MyFragPagerAdapter;
import com.example.chetanps.grocery.R;

public class user_Itemspage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
        String s = getIntent().getStringExtra("Shopname");
        Toast.makeText(this, "You are at shop" + " " + s, Toast.LENGTH_LONG).show();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ViewPager vp = (ViewPager) findViewById(R.id.viewpager);
        this.addPages(vp);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.mTab);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setupWithViewPager(vp);
        tabLayout.setOnTabSelectedListener(listener(vp));

    }

    private void addPages(ViewPager vp) {

        MyFragPagerAdapter adapter = new MyFragPagerAdapter(getSupportFragmentManager());
        adapter.addpage(new allItemsList());
        adapter.addpage(new oilList());

        adapter.addpage(new seedsList());

        vp.setAdapter(adapter);
    }

    private TabLayout.OnTabSelectedListener listener(final ViewPager vp) {
        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vp.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }
}
