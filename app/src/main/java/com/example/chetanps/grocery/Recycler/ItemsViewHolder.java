package com.example.chetanps.grocery.Recycler;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.chetanps.grocery.R;

/**
 * Created by chetanps on 17-03-18.
 */

public class ItemsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    ImageView itemImage;
    TextView itemName;
    ItemClickListener itemClickListener;

Context mcontext;



    public ItemsViewHolder(View itemView,ItemClickListener itemClickListener) {
        super(itemView);

        itemName = (TextView) itemView.findViewById(R.id.itemName);
        itemImage = (ImageView) itemView.findViewById(R.id.itemsImage);
        itemClickListener = itemClickListener;
        itemView.setOnClickListener(this);
    }
//    public void setItemClickListener(ItemClickListener c) {
//        this.itemClickListener = c;
//    }


    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v,getAdapterPosition(),false);
    }
}
