package com.example.chetanps.grocery.Model;

/**
 * Created by chetanps on 19-03-18.
 */

public class Grocery_Items {

    private String Gname;
    private String Gimage;

    public Grocery_Items() {
    }

    public Grocery_Items(String gname, String gimage) {
        Gname = gname;
        Gimage = gimage;
    }

    public String getGname() {
        return Gname;
    }

    public void setGname(String gname) {
        Gname = gname;
    }

    public String getGimage() {
        return Gimage;
    }

    public void setGimage(String gimage) {
        Gimage = gimage;
    }
}
