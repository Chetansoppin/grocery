package com.example.chetanps.grocery.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.app.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.chetanps.grocery.Model.Grocery_Items;
import com.example.chetanps.grocery.R;
import com.example.chetanps.grocery.Recycler.ItemClickListener;
import com.example.chetanps.grocery.Recycler.ItemRecyclerAdapter;
import com.example.chetanps.grocery.Recycler.ItemsAll;
import com.example.chetanps.grocery.Recycler.ViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by chetanps on 17-03-18.
 */

public class seedsList extends Fragment {
    FirebaseAuth auth;
    FirebaseDatabase database;
    DatabaseReference grocery_item;
    RecyclerView rv;
    public ProgressDialog progress;
    FirebaseRecyclerAdapter<Grocery_Items, ViewHolder> adapter2;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.seeds, null);
         rv = (RecyclerView) v.findViewById(R.id.seedsrecylerView);
        database = FirebaseDatabase.getInstance();
        grocery_item = database.getReference("Grocery_Items");
        rv.setLayoutManager(new LinearLayoutManager(this.getActivity()));
//        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
//        rv.setLayoutManager(staggeredGridLayoutManager);
        //   rv.setLayoutManager(new StaggeredGridLayoutManager(this.getActivity(),2));
      //  rv.setAdapter(new ItemRecyclerAdapter(this.getActivity(), getAllItems()));
        loadmenu();
        return v;

    }
    private void loadmenu() {

//FirebaseRecyclerAdapter<Grocery_Items,ViewHolder> all_items_adapter = new FirebaseRecyclerAdapter<Grocery_Items, ViewHolder>(Grocery_Items.class,R.layout.list_layout,ViewHolder.class,grocery_item.orderByKey().equalTo("9060303130")) {
        FirebaseRecyclerAdapter<Grocery_Items, ViewHolder> all_items_adapter = new FirebaseRecyclerAdapter<Grocery_Items, ViewHolder>(Grocery_Items.class, R.layout.list_layout, ViewHolder.class, grocery_item) {

            @Override
            protected void populateViewHolder(ViewHolder viewHolder, Grocery_Items model, int position) {
                viewHolder.txtItemname.setText(model.getGname());
                Picasso.with(getContext()).load(model.getGimage())
                        .into(viewHolder.imgItemImage);
                final Grocery_Items local =model;
                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View v, int positionosition, boolean isLongclick) {

                        Toast.makeText(getContext(),local.getGname(),Toast.LENGTH_LONG).show();
                    }
                });
            }
        };
        rv.setAdapter(all_items_adapter);
    }

    private ArrayList<ItemsAll> getAllItems() {

        ArrayList<ItemsAll> allItems = new ArrayList<>();
        ItemsAll item = new ItemsAll("greenpea", R.drawable.per);
        allItems.add(item);

        ItemsAll item1 = new ItemsAll("peanuts", R.drawable.per);
        allItems.add(item1);

        ItemsAll item2 = new ItemsAll("groundnut", R.drawable.per);
        allItems.add(item2);
        ItemsAll item3 = new ItemsAll("greenpea", R.drawable.per);
        allItems.add(item3);

        ItemsAll item4 = new ItemsAll("peanuts", R.drawable.per);
        allItems.add(item4);

        ItemsAll item5 = new ItemsAll("groundnut", R.drawable.per);
        allItems.add(item5);
        ItemsAll item6 = new ItemsAll("greenpea", R.drawable.per);
        allItems.add(item6);

        ItemsAll item7 = new ItemsAll("peanuts", R.drawable.per);
        allItems.add(item7);

        ItemsAll item8 = new ItemsAll("groundnut", R.drawable.per);
        allItems.add(item8);

        return allItems;
    }

    ;


    @Override
    public String toString() {
        return "Seeds";
    }


}
