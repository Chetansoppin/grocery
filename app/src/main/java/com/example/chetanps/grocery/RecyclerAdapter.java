package com.example.chetanps.grocery;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by chetanps on 16-03-18.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

    ArrayList<model_user> arrayList = new ArrayList<>();

    private ArrayList<String> names;

    RecyclerAdapter(ArrayList<String> names) {
        this.names = names;
    }

    //    RecyclerAdapter(ArrayList<model_user> arrayList)
//    {
//        this.arrayList = arrayList;
//    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shops_lsit, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.c_name.setText(arrayList.get(position).getUsername());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        //ImageView c_flag;
        TextView c_name;

        public MyViewHolder(View itemView) {
            super(itemView);
            //   c_flag = (ImageView) itemView.findViewById(R.id.im_language);
            c_name = (TextView) itemView.findViewById(R.id.shopName);

        }
    }

    public void filterList(ArrayList<String> filterdNames) {
        this.names = filterdNames;
        notifyDataSetChanged();
    }


    public void setFilter(ArrayList<model_user> newList) {

        arrayList = new ArrayList<>();
        arrayList.addAll(newList);
        notifyDataSetChanged();
    }
}
