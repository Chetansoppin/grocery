package com.example.chetanps.grocery.Model;

/**
 * Created by chetanps on 21-03-18.
 */

public class Grocery_itemdetails {
    private String Gname;
    private String Gimage;
    private String Goffer;
    private String Gdescription;
    private String Gprice;

    public Grocery_itemdetails() {
    }

    public Grocery_itemdetails(String gname, String gimage, String goffer, String gdescription, String gprice) {
        Gname = gname;
        Gimage = gimage;
        Goffer = goffer;
        Gdescription = gdescription;
        Gprice = gprice;
    }

    public String getGname() {
        return Gname;
    }

    public void setGname(String gname) {
        Gname = gname;
    }

    public String getGimage() {
        return Gimage;
    }

    public void setGimage(String gimage) {
        Gimage = gimage;
    }

    public String getGoffer() {
        return Goffer;
    }

    public void setGoffer(String goffer) {
        Goffer = goffer;
    }

    public String getGdescription() {
        return Gdescription;
    }

    public void setGdescription(String gdescription) {
        Gdescription = gdescription;
    }

    public String getGprice() {
        return Gprice;
    }

    public void setGprice(String gprice) {
        Gprice = gprice;
    }
}
