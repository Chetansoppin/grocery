package com.example.chetanps.grocery.Recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.chetanps.grocery.R;

/**
 * Created by chetanps on 20-03-18.
 */

public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView txtItemname;
    public ImageView imgItemImage;

    private ItemClickListener itemClickListener;

    public ViewHolder(View itemView) {
        super(itemView);

        txtItemname = (TextView)itemView.findViewById(R.id.itemName);
        imgItemImage = (ImageView)itemView.findViewById(R.id.itemsImage);

        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
       itemClickListener.onClick(v ,getAdapterPosition(),false);

    }
}
