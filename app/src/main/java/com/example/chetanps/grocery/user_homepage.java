package com.example.chetanps.grocery;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.TextView;
import android.widget.Toast;
import com.firebase.ui.database.FirebaseRecyclerAdapter;

import com.example.chetanps.grocery.Grocery.Grocery_item_details;
import com.example.chetanps.grocery.Grocery.user_Itemspage;
import com.example.chetanps.grocery.Model.Grocery_Items;
import com.example.chetanps.grocery.Model.shop_owners;
import com.example.chetanps.grocery.Recycler.ItemClickListener;
import com.example.chetanps.grocery.Recycler.ItemsViewHolder;
import com.example.chetanps.grocery.Recycler.ViewHolder;
import com.example.chetanps.grocery.Recycler.shopowner_holder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.ArrayList;

//import br.com.mauker.materialsearchview.MaterialSearchView;

public class user_homepage extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    ListView lv_languages;
    private static LayoutInflater inflater = null;
    public FirebaseAuth auth;
    public String userEmailId;
    EditText inputSearch;
    FirebaseDatabase database;
    DatabaseReference shopOwners;

//    private DatabaseReference mDatabase;
TextView shopName,shopNumber,shopAddress;
RecyclerView rv;
    public ArrayList<user_model> arraylist;
    FirebaseRecyclerAdapter<Grocery_Items, ItemsViewHolder> adapter1;

    user_shopsAdpterclass list_adapter;
    String[] languages = new String[]{"A.G.PROVISION STORE",
            "K P STORES",
            "M J INDUSTRIES",
            "J K GENEREL STORES",
            "K M P ",
            "G H PROVISIONS",
            "HUBLI STORES",
            "GOKUL STORES",
            "SHETTAR PROVISION"
    };

    public static int[] language_images = {R.drawable.per,
            R.drawable.per,
            R.drawable.per,
            R.drawable.per,
            R.drawable.per,
            R.drawable.per,
            R.drawable.per,
            R.drawable.per,
            R.mipmap.ic_launcher};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_homepage);

        rv = (RecyclerView) findViewById(R.id.listShops);
        shopName = (TextView) findViewById(R.id.shopName);
        shopNumber = (TextView) findViewById(R.id.shopNumber);
        shopAddress = (TextView) findViewById(R.id.shopAddress);
        database = FirebaseDatabase.getInstance();
        shopOwners = database.getReference("Shop_owners");
        rv.setLayoutManager(new LinearLayoutManager(this));
        auth = FirebaseAuth.getInstance();
        FirebaseUser user1 = FirebaseAuth.getInstance().getCurrentUser();
//
//        if (user1 != null) {
//            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
//            userEmailId = user.getEmail().toString().trim();
//        }


        Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.user);
        Bitmap circularBitmap = user_imagehelper.getRoundedCornerBitmap(bitmap, 100);



        NavigationView navigationView1 = (NavigationView) findViewById(R.id.nav_view);
        View headerview = navigationView1.getHeaderView(0);

      //  mDatabase = FirebaseDatabase.getInstance().getReference();

        ImageView user_drawerimage = (ImageView) headerview.findViewById(R.id.imageView);
        TextView user_email = (TextView) headerview.findViewById(R.id.userName);
        user_email.setText(userEmailId);
        user_drawerimage.setImageBitmap(circularBitmap);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);





      //  init();
  //      lv_languages.setAdapter(list_adapter);
    //    lv_languages.setTextFilterEnabled(true);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  searchView.openSearch();
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        loadmenu();

    }

    private void loadmenu() {

        final FirebaseRecyclerAdapter<shop_owners,shopowner_holder> all_items_adapter = new FirebaseRecyclerAdapter<shop_owners, shopowner_holder>(shop_owners.class, R.layout.shops_lsit, shopowner_holder.class, shopOwners) {

            @Override
            protected void populateViewHolder(shopowner_holder viewHolder, shop_owners model, int position) {

                viewHolder.shopName.setText(model.getShop_name());
                viewHolder.shopNumber.setText(model.getShop_phone());
                viewHolder.shopAddress.setText(model.getShop_address());
//                Picasso.with(getContext()).load(model.getGimage())
//                        .into(viewHolder.imgItemImage);

                final shop_owners local = model;
                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View v, int position, boolean isLongclick) {
                        Intent shop_in = new Intent(getApplicationContext(), user_Itemspage.class);
                        shop_in.putExtra("selectedshop", local.getOwner_phone());
                        Toast.makeText(getApplicationContext(), local.getShop_address(), Toast.LENGTH_LONG).show();

                        Toast.makeText(getApplicationContext(), local.getShop_name(), Toast.LENGTH_LONG).show();
                        startActivity(shop_in);
                    }
                });
            }
        };
        rv.setAdapter(all_items_adapter);

    }
//
//    private void init() {
//
//
//
//
//
//
//
//
//
//
//            list_adapter = new user_shopsAdpterclass(this, languages, language_images);
//        list_adapter.notifyDataSetChanged();
//        lv_languages = (ListView) findViewById(R.id.shopslist);
//        lv_languages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                String cities = parent.getItemAtPosition(position).toString();
//                Toast.makeText(user_homepage.this, cities, Toast.LENGTH_LONG).show();
//                String Shop_name = parent.getItemAtPosition(position).toString();
//
//
//                Intent intent = new Intent(getApplicationContext(), user_Itemspage.class);
//                intent.putExtra("Shopname", Shop_name);
//                startActivity(intent);
//
//            }
//        });
//    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
//
//    private void writeNewUser() {
//        //  FirebaseUser user1 = FirebaseAuth.getInstance().getCurrentUser();
//        user_model user = new user_model("name1", "password1", "pic user1");
//
//        mDatabase.child("Users").child("test_user1").setValue(user);
//    }
//
//    private void ReadUser() {
//        mDatabase = FirebaseDatabase.getInstance().getReference("Users");
//        mDatabase.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                dataSnapshot.child("Users");
//                String jshd = DataSnapshot.class.getName();
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user_homepage, menu);
        //MenuItem item = menu.findItem(R.id.action_search);
        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        //  searchView.setOnQueryTextListener(this);


        //SearchManager searchManager = (SearchManager) user_homepage.this.getSystemService(Context.SEARCH_SERVICE);


//        SearchView searchView = null;
//        if (searchItem != null) {
//            searchView = (SearchView) searchItem.getActionView();
//          //  searchView.setOnQueryTextListener(this);
//        }
//        if (searchView != null) {
//            searchView.setSearchableInfo(searchManager.getSearchableInfo(user_homepage.this.getComponentName()));
//        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.userHome) {
            MDToast mdToast = MDToast.makeText(this, "this is the home page");
            mdToast.show();

            // Handle the camera action
        } else if (id == R.id.notifications) {
            MDToast mdToast = MDToast.makeText(this, "clicked on notification");
            mdToast.show();


        } else if (id == R.id.orderHistory) {
            MDToast mdToast = MDToast.makeText(this, "clicked on orderHistory");
            mdToast.show();

        } else if (id == R.id.userOffers) {
            MDToast mdToast = MDToast.makeText(this, "clicked on userOffers");
            mdToast.show();

        } else if (id == R.id.userSettings) {
            MDToast mdToast = MDToast.makeText(this, "clicked on userSettings");
            mdToast.show();

        } else if (id == R.id.userProfile) {
            MDToast mdToast = MDToast.makeText(this, "clicked on userProfile");
            mdToast.show();

        } else if (id == R.id.userLogout) {
            auth.signOut();
            //   Auth.GoogleSignInApi.signOut(GoogleSignInClient apiclient)
            startActivity(new Intent(user_homepage.this, user_login.class));

            MDToast mdToast = MDToast.makeText(this, "clicked on userLogout");
            mdToast.show();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
//
//    @Override
//    public boolean onQueryTextSubmit(String query) {
//        return false;
//    }
//
//    @Override
//    public boolean onQueryTextChange(String newText) {
//
//        newText = newText.toLowerCase();
//        ArrayList<user_model> newList = new ArrayList<>();
//        for(user_model users :arraylist){
//String name = users.getUsername().toLowerCase();
//if(name.contains(newText))
//    newList.add(users);
//        }
//        list_adapter.setFilter(newList);
//        return true;
//    }
}
