package com.example.chetanps.grocery.Recycler;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by chetanps on 23-03-18.
 */

public class viewholder_itemdetails {

    public TextView itemName;
    public ImageView itemImage;
    public TextView itemDescription;
    public  TextView itemOffer;
    public  TextView itemPrice;

    public viewholder_itemdetails() {

    }
}
