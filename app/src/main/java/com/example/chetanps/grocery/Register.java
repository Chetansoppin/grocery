package com.example.chetanps.grocery;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Register extends AppCompatActivity implements View.OnClickListener {

    EditText email, password, username;
    Button register;
    public FirebaseAuth mauth;
    public String UserEmail, Userpassword, Username;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        username = (EditText) findViewById(R.id.name);
        register = (Button) findViewById(R.id.Register);
        mauth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        FirebaseUser user1 = FirebaseAuth.getInstance().getCurrentUser();

        if (user1 != null) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            String userUid = user.getUid();
            String userEmail = user.getEmail();
            startActivity(new Intent(Register.this, user_homepage.class));

        }
        register.setOnClickListener(this);
    }
//    private void writeNewUser() {
//        user_model user = new user_model(username.getText().toString(),email.getText().toString(),"https://www.seeandsay.in/57308/rakshit-shetty-celebrates-best-birthday");
//
//        mDatabase.child("users").setValue(user);
//    }

    @Override
    public void onClick(View v) {
        UserEmail = email.getText().toString().trim();
        Username = username.getText().toString().trim();
        Userpassword = password.getText().toString().trim();

        try {
            mauth.createUserWithEmailAndPassword(UserEmail, Userpassword)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            Toast.makeText(Register.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                            //   progressBar.setVisibility(View.GONE);
                            // If sign in fails, display a message to the user. If sign in succeeds
                            // the auth state listener will be notified and logic to handle the
                            // signed in user can be handled in the listener.
                            if (!task.isSuccessful()) {
                                Toast.makeText(Register.this, "Authentication failed." + task.getException(),
                                        Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(Register.this, "Authentication done.",
                                        Toast.LENGTH_LONG).show();
                                //  writeNewUser();
                                startActivity(new Intent(Register.this, user_login.class));
                                finish();
                            }
                        }
                    });
        } catch (Exception ex) {
            Toast.makeText(this, ex.toString(), Toast.LENGTH_LONG).show();
        }
    }
};