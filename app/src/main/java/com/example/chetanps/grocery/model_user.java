package com.example.chetanps.grocery;

/**
 * Created by chetanps on 16-03-18.
 */

public class model_user {
    public String username;

    public model_user() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public model_user(String username) {
        this.username = username;

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
