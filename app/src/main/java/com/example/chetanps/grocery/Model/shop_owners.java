package com.example.chetanps.grocery.Model;

/**
 * Created by chetanps on 23-03-18.
 */

public class shop_owners {
    public String owner_name;
    public String owner_email;
    public String owner_phone;
    public String shop_address;
    public String shop_name;
    public String shop_phone;

    public shop_owners() {
    }

    public shop_owners(String owner_name, String owner_phone, String shop_address, String shop_name, String shop_phone,String owner_email) {
        owner_name = owner_name;
        this.owner_email = owner_email;
        owner_phone = owner_phone;
        this.shop_address = shop_address;
        this.shop_name = shop_name;
        this.shop_phone = shop_phone;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public String getOwner_email() {
        return owner_email;
    }

    public void setOwner_email(String owner_email) {
        this.owner_email = owner_email;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getOwner_phone() {
        return owner_phone;
    }

    public void setOwner_phone(String owner_phone) {
        this.owner_phone = owner_phone;
    }

    public String getShop_address() {
        return shop_address;
    }

    public void setShop_address(String shop_address) {
        this.shop_address = shop_address;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getShop_phone() {
        return shop_phone;
    }

    public void setShop_phone(String shop_phone) {
        this.shop_phone = shop_phone;
    }
}
