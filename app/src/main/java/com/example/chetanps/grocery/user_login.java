package com.example.chetanps.grocery;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.valdesekamdem.library.mdtoast.MDToast;

public class user_login extends AppCompatActivity implements View.OnClickListener {

    EditText userEmail, userPassword;
    Button login;
    TextView mainText,subText,registerLink;
    public FirebaseAuth auth;
    public String UserEmail, UserPassword, Username;
    FirebaseDatabase dt;
    DatabaseReference reference;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);

        userEmail = (EditText) findViewById(R.id.userEmail);
        userPassword = (EditText) findViewById(R.id.userPassword);
        login = (Button) findViewById(R.id.Login);
        progressDialog = new ProgressDialog(this);
        mainText = (TextView)findViewById(R.id.mainText);
        subText = (TextView) findViewById(R.id.subText);
        registerLink = (TextView) findViewById(R.id.textViewLinkRegister);

        Typeface type = Typeface.createFromAsset(getAssets(),"Nabila.ttf");
        mainText.setTypeface(type);
        subText.setTypeface(type);
        auth = FirebaseAuth.getInstance();
        dt = FirebaseDatabase.getInstance();
        reference = dt.getReference("Grocery_items");

        login.setOnClickListener(this);
        //  Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //  setSupportActionBar(toolbar);
    }

public  void  onRegClick(View v){
        Intent regAct = new Intent(this, Register.class);
        startActivity(regAct);
}


    @Override
    public void onClick(View v) {
        progressDialog.setMessage("logging in Please Wait...");
        progressDialog.show();

        UserEmail = userEmail.getText().toString().trim();
        UserPassword = userPassword.getText().toString().trim();

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
           if(!dataSnapshot.child("9060303130").exists()){
               return;
              // Toast.makeText(this,"user not exist",Toast.LENGTH_LONG).show();
           }else {
               //
           }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        try {
            auth.signInWithEmailAndPassword(UserEmail, UserPassword)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                         //   Toast.makeText(user_login.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                            //   progressBar.setVisibility(View.GONE);
                            // If sign in fails, display a message to the user. If sign in succeeds
                            // the auth state listener will be notified and logic to handle the
                            // signed in user can be handled in the listener.
                            if (!task.isSuccessful()) {
                                progressDialog.dismiss();
//                                Toast.makeText(user_login.this, "Authentication failed." + task.getException(),
//                                        Toast.LENGTH_LONG).show();

                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(user_login.this, "Authentication done.",
                                        Toast.LENGTH_LONG).show();
                                startActivity(new Intent(user_login.this, user_homepage.class));
                                finish();
                            }

                        }
                    })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                    if(e instanceof FirebaseAuthInvalidCredentialsException){
                        MDToast mdToast = MDToast.makeText(getApplicationContext(), "Invalid password");
                        mdToast.show();
                    }
                    if(e instanceof FirebaseAuthInvalidUserException){
                        String errorCode =
                                ((FirebaseAuthInvalidUserException) e).getErrorCode();

                        if (errorCode.equals("ERROR_USER_NOT_FOUND")) {
                            MDToast mdToast = MDToast.makeText(getApplicationContext(), "No matching account found");
                            mdToast.show();
                        } else if (errorCode.equals("ERROR_USER_DISABLED")) {
                            MDToast mdToast = MDToast.makeText(getApplicationContext(), "User account has been disabled");
                            mdToast.show();

                        } else {
                            MDToast mdToast = MDToast.makeText(getApplicationContext(), "Invalid user");
                            mdToast.show();
                        }
                    }


                }
            });
        } catch (Exception ex) {
            Toast.makeText(this, ex.toString(), Toast.LENGTH_LONG).show();
        }
    }

}
