package com.example.chetanps.grocery.Recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.chetanps.grocery.R;

/**
 * Created by chetanps on 23-03-18.
 */

public class shopowner_holder extends RecyclerView.ViewHolder implements View.OnClickListener {


    public TextView shopName;
    public TextView shopAddress;
    public TextView shopNumber;
    private TextView shopOwner;
    ItemClickListener itemClickListener;


    public shopowner_holder(View itemView) {
        super(itemView);
        shopName = (TextView) itemView.findViewById(R.id.shopName);
        shopNumber = (TextView) itemView.findViewById(R.id.shopNumber);
        shopAddress = (TextView) itemView.findViewById(R.id.shopAddress);
        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v,getAdapterPosition(),false);

    }
}
