package com.example.chetanps.grocery.Recycler;

/**
 * Created by chetanps on 17-03-18.
 */

public class ItemsAll {
    private String itemName;

    private   int itemImage;

    public ItemsAll(String itemName, int itemImage) {
        this.itemName = itemName;
        this.itemImage = itemImage;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getItemImage() {
        return itemImage;
    }

    public void setItemImage(int itemImage) {
        this.itemImage = itemImage;
    }
}
