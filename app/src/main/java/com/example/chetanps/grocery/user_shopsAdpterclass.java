package com.example.chetanps.grocery;

import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by chetanps on 13-03-18.
 */

public class user_shopsAdpterclass extends BaseAdapter {

    //    private ArrayList<MyBean> myList;  // for loading main list
//    private ArrayList<MyBean> arraylist=null;
    String[] result;
    Context context;
    int[] imageId;
    Context mcontext;
    private static LayoutInflater inflater = null;

    public user_shopsAdpterclass(user_homepage homePage, String[] prgmNameList, int[] prgmImages) {
// TODO Auto-generated constructor stub
        result = prgmNameList;
        context = homePage;
        imageId = prgmImages;

//        this.arraylist = new ArrayList<MyBean>();
//        this.arraylist.addAll(myList);

        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return result.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    public class Holder {
        TextView tv_language;
        ImageView im_language;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        Holder holder = new Holder();
        View view;
        view = inflater.inflate(R.layout.shops_lsit, null);

        holder.tv_language = (TextView) view.findViewById(R.id.shopName);
        holder.im_language = (ImageView) view.findViewById(R.id.im_language);

        holder.tv_language.setText(result[position]);
        Picasso.with(context).load(imageId[position]).into(holder.im_language);

//        view.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//// TODO Auto-generated method stub
//                Toast.makeText(context, "You Clicked " + result[position], Toast.LENGTH_LONG).show();
////                Intent intent = new Intent(parent.getContext(), user_Itemspage.class);
////                intent.putExtra("Shopname", result[position]);
////                mcontext.startActivity(intent);
//            }
//        });
        return view;
    }

    public void setFilter(ArrayList<user_model> newList) {

        ArrayList arrayList = new ArrayList<>();
        arrayList.add(newList);
        notifyDataSetChanged();

    }


//    public void getFilter(String charText) {
//        charText = charText.toLowerCase(Locale.getDefault());
//        myList.clear();
//        if (charText.length() == 0) {
//            myList.addAll(arraylist);
//        }
//        else
//        {
//            for (MyBean wp : arraylist) {
//                if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
//                    myList.add(wp);
//                }
//            }
//        }
//        notifyDataSetChanged();
//    }
}

