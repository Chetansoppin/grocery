package com.example.chetanps.grocery.Grocery;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.chetanps.grocery.Model.Grocery_itemdetails;
import com.example.chetanps.grocery.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Grocery_item_details extends AppCompatActivity {

    String selectedItem="";
    ElegantNumberButton button;
    FirebaseAuth auth;
    FirebaseDatabase database;
    DatabaseReference selected_itemdetails;
ImageView selected_item_image;
TextView selected_item_name,selected_item_description,selected_item_offer,selected_item_price;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grocery_itemdetails);
        selected_item_name = (TextView) findViewById(R.id.itemsName);
        selected_item_image = (ImageView) findViewById(R.id.itemsImage);
        selected_item_description = (TextView) findViewById(R.id.itemDescription);
        selected_item_offer = (TextView) findViewById(R.id.itemOffer);
        selected_item_price = (TextView) findViewById(R.id.itemPrice);
        if(getIntent()!=null){
            selectedItem =getIntent().getStringExtra("selectedItem");
            Toast.makeText(this,selectedItem,Toast.LENGTH_LONG).show();
        }
        database = FirebaseDatabase.getInstance();
        selected_itemdetails = database.getReference("Grocery_ItemDetails");
        selected_itemdetails.child("9060303130").child(selectedItem).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Grocery_itemdetails post = dataSnapshot.getValue(Grocery_itemdetails.class);
                String s = post.getGname().toString();
    selected_item_name.setText(post.getGname().toString());
//    selected_item_image.setImageResource(post.getGimage().toString());
                selected_item_description.setText(post.getGdescription().toString());
                selected_item_offer.setText(post.getGoffer().toString());
                selected_item_price.setText(post.getGprice().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        button  = (ElegantNumberButton) findViewById(R.id.number_button);
        button.setOnClickListener(new ElegantNumberButton.OnClickListener() {
            @Override
            public void onClick(View view) {
                String num = button.getNumber();
               // Toast.makeText(getBaseContext(),selected_itemdetails)
                Toast.makeText(Grocery_item_details.this, num, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
