package com.example.chetanps.grocery.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created by chetanps on 26-03-18.
 */

public class Database extends SQLiteAssetHelper {

    public static final String DB_NAME = "Orders.db";
    public static final int DB_VER = 1;

    public Database(Context context) {
        super(context, DB_NAME,null,DB_VER);
    }

    public Database(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }
}
