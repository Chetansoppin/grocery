package com.example.chetanps.grocery.Recycler;

import android.view.View;
import android.widget.Toast;

/**
 * Created by chetanps on 17-03-18.
 */

public interface ItemClickListener {
    void onClick(View v, int positionosition,boolean isLongclick);
}
