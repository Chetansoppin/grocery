package com.example.chetanps.grocery;

/**
 * Created by chetanps on 14-03-18.
 */

public class user_model {

    public String username;
    public String email;
    public String photoUrl;
    public int pic;

    public user_model() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public user_model(String username, String email, String photoUrl) {
        this.username = username;
        this.email = email;
//        this.pic = pic;
        this.photoUrl = photoUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public void setPic(int pic) {
        this.pic = pic;
    }
}
